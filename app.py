from flask import Flask, g, redirect, request, url_for

from api import users, posts, csp, cli_api
from business import bad_session
from etc import settings
from etc.csp import CSP_HEADERS
from etc.settings import HOST, PORT

app = Flask('my_insecure_app')

app.config['SECRET_KEY'] = settings.SECRET_KEY

app.register_blueprint(users.api_user, url_prefix='/user')

app.register_blueprint(posts.api_posts, url_prefix='/posts')
app.register_blueprint(csp.api_csp, url_prefix='/csp')
app.register_blueprint(cli_api.cli_api, url_prefix='/cli_api')


def add_csp_headers(response):
    response.headers['Content-Security-Policy'] = CSP_HEADERS
    return response


app.after_request(add_csp_headers)


@app.route('/')
def index():
    return redirect(url_for('posts.show_post'))


@app.before_request
def before_request():
    g.session = bad_session.load_user_session(request)


if __name__ == '__main__':
    app.run(host=HOST, port=PORT, debug=True)
