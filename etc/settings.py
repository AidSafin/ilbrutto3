import os

HOST = '0.0.0.0'
PORT = 5000
SECRET_KEY = 'the_secrest_secret_ever'

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DB_PATH = f'{BASE_DIR}/.data/user.sqlite'
