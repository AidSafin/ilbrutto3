CSP_HEADERS = "script-src 'self' 'unsafe-inline' https://apis.google.com; " \
              "connect-src 'self' https://*.ipify.org; img-src 'self' https://www.python.org;  " \
              "style-src 'self' 'unsafe-inline' https://fonts.googleapis.com; " \
              "font-src 'self' https://fonts.gstatic.com; "
