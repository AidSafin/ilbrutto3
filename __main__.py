import click

from cli.db import db_init
from cli.do_things import say_message_to_user


@click.group()
def cli():
    pass


cli.add_command(db_init)
cli.add_command(say_message_to_user)

if __name__ == '__main__':
    cli()
