from flask import Blueprint, request, jsonify
from jsonschema import validate, ValidationError

from business import auth, post

cli_api = Blueprint('cli_api', __name__, template_folder='templates')

key_schema = {
    "type": "object",
    "required": ["username", "password"],
    "properties": {
        "username": {"type": "string"},
        "password": {"type": "string"},
    },
}

post_schema = {
    "type": "object",
    "required": ["text"],
    "properties": {"text": {"type": "string"}, },
}


@cli_api.route('/key', methods=['POST'])
def do_key_create():
    data = request.get_json()

    try:
        validate(data, key_schema)
    except ValidationError:
        return jsonify({'error': 'invalid schema', 'schema': key_schema}), 400

    key = auth.keygen(data['username'], data['password'])
    if key:
        return jsonify({'key': key}), 200
    return jsonify({'error': 'invalid login'}), 403


@cli_api.route('/post/<username>', methods=['GET'])
def do_post_list(username):
    posts = post.get_posts(username)
    return jsonify(posts)


@cli_api.route('/post', methods=['POST'])
def do_post_create():

    data = {'username': auth.authenticate(request)}
    if not data['username']:
        return jsonify({'error': 'invalid authentication'}), 401

    data.update(request.get_json())
    try:
        validate(data, post_schema)
    except ValidationError:
        return jsonify({'error': 'invalid schema', 'schema': post_schema}), 400

    post.make_post(data['username'], data['text'])
    return "Пост-то создан!"
