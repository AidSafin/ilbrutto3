from flask import Blueprint, render_template

api_csp = Blueprint('api_csp', __name__, template_folder='templates')


@api_csp.route('/', methods=['GET'])
def do_csp():
    return render_template('csp.html')
