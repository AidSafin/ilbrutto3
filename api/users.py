from flask import Blueprint, render_template, redirect, request, g, session, make_response, flash, url_for

from business import user, bad_session

api_user = Blueprint('user', __name__, template_folder='templates')


@api_user.route('/login', methods=['GET', 'POST'])
def do_login():
    session.get('username')

    if request.method == 'POST':
        username = request.form.get('username')
        password = request.form.get('password')

        username = user.login(username, password)
        if not username:
            flash("Invalid user or password")
            return render_template('user.login.html')

        response = make_response(redirect(url_for('index')))
        response = bad_session.create(response=response, username=username)
        return response

    return render_template('user.login.html')


@api_user.route('/create', methods=['GET', 'POST'])
def do_create():

    session.pop('username', None)

    if request.method == 'POST':

        username = request.form.get('username')
        password = request.form.get('password')
        if not username or not password:
            flash("Please, complete username and password")
            return render_template('user.create.html')

        user.create(username, password)
        flash("User created. Please login.")
        return redirect(url_for('user.do_login'))

    return render_template('user.create.html')


@api_user.route('/change_password', methods=['GET', 'POST'])
def change_password():

    if request.method == 'POST':

        password = request.form.get('password')
        password_again = request.form.get('password_again')

        if password != password_again:
            flash("The passwords don't match")
            return render_template('user.chpasswd.html')

        if not user.is_password_strong(password):
            flash("Слишком слабенький пароль")
            return render_template('user.chpasswd.html')

        user.password_change(g.session['username'], password)
        flash("Пароль успешно изменен")

    return render_template('user.chpasswd.html')
