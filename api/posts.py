from flask import Blueprint, render_template, redirect, request, g, url_for

from business import user, post

api_posts = Blueprint('posts', __name__, template_folder='templates')


@api_posts.route('/')
@api_posts.route('/<username>')
def show_post(username=None):
    if not username and 'username' in g.session:
        username = g.session['username']
    posts = post.get_posts(username)
    users = user.userlist()
    return render_template('posts.view.html', posts=posts, username=username, users=users)


@api_posts.route('/', methods=['POST'])
def create_post():
    if 'username' not in g.session:
        return redirect(url_for('user.do_login'))
    username = g.session['username']
    text = request.form.get('text')
    post.make_post(username, text)
    return redirect(url_for('index'))
