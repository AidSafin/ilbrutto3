FROM python:3.9-slim

RUN echo ' ---> Fixing, extending sources and installing build dependencies' && \
    apt-get update && \
    apt-get install --no-install-recommends --yes curl vim

ENV LANG=en_US.UTF-8 \
    PYTHONPATH=/usr/src/app

WORKDIR /usr/src/app
COPY ./Pipfile* ./

RUN echo ' ---> Installing python dependencies' && \
    ln -sf /usr/local/bin/python /bin/python && \
    pip install --upgrade pipenv && \
    pipenv install --system --deploy

EXPOSE 5000

COPY . .
