import click
import requests
from pathlib import Path

from etc.settings import HOST, PORT

api_key_file_path = Path('/tmp/supersecret.txt')


@click.command('say')
@click.argument('message')
def say_message_to_user(message):
    if not api_key_file_path.exists():

        username = click.prompt('Username')
        password = click.prompt('Password')

        r = requests.post(f'http://{HOST}:{PORT}/cli_api/key', json={'username': username, 'password': password})

        if r.status_code != 200:
            click.echo(f'Я не смог: {r.status_code} {r.text}')
            return False

        api_key = r.json()['key']
        click.echo(f'Получен ключ: {api_key}')

        with api_key_file_path.open('w') as outfile:
            outfile.write(api_key)

    api_key = api_key_file_path.open().read()
    r = requests.post(f'http://{HOST}:{PORT}/cli_api/post', json={'text': message}, headers={'X-APIKEY': api_key})
    if r.status_code != 200:
        click.echo(f'Я опять не смог: {r.status_code} {r.text}')
        return False
