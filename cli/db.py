import sqlite3

import click
from passlib.hash import sha1_crypt

from etc.settings import DB_PATH


@click.command('db-init')
def db_init():

    users = [
        ('admin', sha1_crypt.hash('123456')),
        ('JohnPizhon', sha1_crypt.hash('Password')),
        ('Dart', sha1_crypt.hash('BezVeder')),
    ]
    conn = sqlite3.connect(f'{DB_PATH}')
    c = conn.cursor()
    c.execute("DROP TABLE IF EXISTS users")
    c.execute("DROP TABLE IF EXISTS posts")
    c.execute("CREATE TABLE users (user text, password text, failures int)")
    c.execute("CREATE TABLE posts (date date, username text, text text)")

    for u, p in users:
        c.execute("INSERT INTO users (user, password, failures) VALUES ('%s', '%s', '%d')" % (u, p, 0))

    conn.commit()
    conn.close()
