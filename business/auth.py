import random
import hashlib

from pathlib import Path

from business import user


def keygen(username, password=None):

    if password:
        if not user.login(username, password):
            return None

    key = hashlib.sha1(str(random.getrandbits(1024)).encode()).hexdigest()

    for f in Path('/tmp/').glob('insecure_app.apikey.' + username + '.*'):
        print('Удаляю', f)
        f.unlink()

    keyfile = '/tmp/insecure_app.apikey.{}.{}'.format(username, key)

    Path(keyfile).touch()

    return key


def authenticate(request):
    if 'X-APIKEY' not in request.headers:
        return None

    key = request.headers['X-APIKEY']

    for f in Path('/tmp/').glob('insecure_app.apikey.*.' + key):
        return f.name.split('.')[2]

    return None
