import sqlite3

from etc.settings import DB_PATH


def get_posts(username):
    conn = sqlite3.connect(DB_PATH)
    conn.row_factory = sqlite3.Row
    c = conn.cursor()
    rows = c.execute("SELECT * FROM posts WHERE username = ? ORDER BY date DESC", (username, )).fetchall()
    posts = [dict(zip(row.keys(), row)) for row in rows]
    return posts


def make_post(username, text):
    conn = sqlite3.connect(DB_PATH)
    conn.row_factory = sqlite3.Row
    c = conn.cursor()
    c.execute("INSERT INTO posts (username, text, date) VALUES (?, ?, DateTime('now'))", (username, text))
    conn.commit()
