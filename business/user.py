import sqlite3

from etc.settings import DB_PATH
from passlib.hash import sha1_crypt


def login(username, password):
    conn = sqlite3.connect(DB_PATH)
    conn.row_factory = sqlite3.Row
    conn.set_trace_callback(print)
    c = conn.cursor()
    user = c.execute(f"SELECT * FROM users WHERE user = '{username}'").fetchone()
    if user and sha1_crypt.verify(password, user['password']):
        return user['user']
    return False


def create(username, password):

    conn = sqlite3.connect(DB_PATH)
    enc_password = sha1_crypt.hash(password)
    c = conn.cursor()

    c.execute("INSERT INTO users (user, password, failures) VALUES ('%s', '%s', '%d')" % (username, enc_password, 0))

    conn.commit()
    conn.close()


def userlist():
    conn = sqlite3.connect(DB_PATH)
    conn.row_factory = sqlite3.Row
    c = conn.cursor()
    users = c.execute("SELECT * FROM users").fetchall()
    return [] if not users else [user['user'] for user in users]


def password_change(username, password):
    enc_password = sha1_crypt.hash(password)
    conn = sqlite3.connect(DB_PATH)
    conn.set_trace_callback(print)
    conn.row_factory = sqlite3.Row
    c = conn.cursor()
    c.execute(f"UPDATE users SET password = '{enc_password}' WHERE user = '{username}'")
    conn.commit()


def is_password_strong(password):
    return True
